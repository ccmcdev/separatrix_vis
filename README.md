# README #
Use this repo to visualize output from Alex Glocer's separatrix analysis.
Alex's code produces very high resolution meshes of size 250x500, which take a long time to visualize in the browser. I have implemented a mesh reduction scheme that allows the user to select the resolution of the output mesh.

##Setup and Install##

Download the vis code.
```
#!shell
git clone https://bitbucket.org/ccmcdev/separatrix_vis.git
```

### Miniconda ###
Miniconda will be used to set up virtual environments and get requirements

[Download](http://conda.pydata.org/miniconda.html) minconda installer.

run installer (mac OSX shown here) and enter through prompts

```
#!shell

bash Miniconda-latest-MacOSX-x86_64.sh

```

Create virtual miniconda environment on linux:
```
#!
cd separatrix_vis
conda create --name separatrix_visualizer --file requirements_linux.txt
```
You can now run python (and visualization script) from within the virtual environment.
```
#!shell
source activate separatrix_visualizer
(separatrix_visualizer)$ python Glocer_visualizer.py -h
```

If you need to run outside an environment, first deactivate the environment
```
#!shell
source deactivate
```
note the path to separatrix_visualizer environment:

```
#!shell

conda info --envs
```
in my case this returns:
```
#!shell
# conda environments:
#
separatrix_visualizer     /Users/apembrok/miniconda2/envs/separatrix_visualizer
viva                     /Users/apembrok/miniconda2/envs/viva
root                  *  /Users/apembrok/miniconda2

```

The python executable is /path/to/miniconda2/envs/separatrix_visualizer/bin/python


## Usage ##
Invoke the python executable that has all the proper requirements linked:
```
#!shell
(separatrix_visualizer)$ python Glocer_visualizer.py -h
usage: Glocer_visualizer.py [-h] [-input_file input_data_file]
                            [-glocer_dir full/path/to/glocer_dir]
                            [-res ni [nj ...]] [-fig_size width [height ...]]
                            [-auto_open] [-out_dir OUTPUT_DIRECTORY]
                            [-show_earth]

Visualizes results from Glocer Separatrix Finder.

optional arguments:
  -h, --help            show this help message and exit
  -input_file input_data_file
                        name of data file used to generate results
  -glocer_dir full/path/to/glocer_dir
                        path to output directory of glocer analysis
  -res ni [nj ...], --resolution ni [nj ...]
                        resolution of the vis meshes
  -fig_size width [height ...], --figure_size width [height ...]
                        shape of the figure in pixels
  -auto_open, --auto_open_in_browser
                        opens the visualization in the browser
  -out_dir OUTPUT_DIRECTORY, --output_directory OUTPUT_DIRECTORY
                        directory in which to place visualizer. If not
                        specified, visualizer will be placed in glocer_dir
  -show_earth           include a rendering of earth in ouput
```


## Example ##

```
#!shell

python Glocer_visualizer.py sample_data/ 3d__var_1_e20150317-010100-000.out -res 50 50 -auto_open -out_dir ./
```

Above call generates the file 3d__var_1_e20150317-010100-000.out_Visualizer.html and saves it the current directory. It then opens the visualizer automatically in the browser. If you don't want your browser to open, leave off the -auto_open flag.

```
#!shell

python Glocer_visualizer.py -glocer_dir /Users/apembrok/Work/Separatrix/Glocer_method/Separatrix_Visualizer/sample_data/ -res 50 50 -auto_open
```
This call generates a time series, assuming multiple time steps are included in -glocer_dir. 

## Update ##
I have made this repository public, so you should be able to pull updates without an account.

```
#!shell

git pull
```