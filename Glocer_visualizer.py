import os,sys, argparse
from pandas import DataFrame as df
import pandas as pd
import numpy as np
import plotly.graph_objs as go
from scipy.interpolate import RectBivariateSpline
from plotly.offline import plot
from earth_vis import earth_plot
import satellites_vis as Sat_vis
import json
from plotly.utils import PlotlyJSONEncoder
from collections import OrderedDict
import colorlover as cl


def read_tecplot(filename, dtype = np.dtype(np.float64), sep = ' '):
    with open(filename, 'r') as f:
        variables = f.readline().split("\"")
        names = [variables[i] for i in range(1,len(variables),2)]
        zone = f.readline()
        ni = int(zone.split(",")[0].split('=')[-1])
        nj = int(zone.split(",")[1].split('=')[-1])
        if ni*nj == 1:
            point = pd.read_csv(f,names=names,sep=sep,skipinitialspace= True)
            return point
        else:
            table = pd.read_table(f, names = names, sep = sep, skipinitialspace = True)
            table.ni = ni
            table.nj = nj
            return table

def reduce_surface(surface, ni,nj):
    '''Reduces a 2D surface to ni, nj resolution. 
        Assumes surface is a dictionary with 
            -keys representing components in n-d space
            -attributes ni,nj representing previous resolution
        Returns list of new components with shape ni,nj
        '''
    new_surface = {}
    mesh = np.meshgrid(np.linspace(0,1.,ni),np.linspace(0,1.,nj),indexing = 'ij')
    
    # create a parameterization on [0,1]
    i = 1.*np.arange(surface.ni)/(surface.ni-1)
    j = 1.*np.arange(surface.nj)/(surface.nj-1)
    
    for k in surface.keys():
        component = surface[k].reshape((surface.nj,surface.ni))
        rbs_component = RectBivariateSpline(j,i,component)
        new_surface[k] = rbs_component.ev(mesh[0].ravel(),mesh[1].ravel())
    new_surface = df(new_surface)
    new_surface.ni = ni
    new_surface.nj = nj

    return new_surface



def plot_scatter(x,y,z, color =None, size = 1, name = None, mode = 'markers', visible= True, opacity = .8, legendgroup='',showlegend=True, line_color = None):
    if color is None:
        marker=dict(
            size=size,
            opacity=opacity
        )
    else:
        marker=dict(
            size=size,
            color = color,
            colorscale='Viridis',
            opacity=opacity,
            line = {'width':2}
        )

    if line_color is None:
        line= dict(color=color, width=size)
    else:
        line= dict(color=line_color, width = size)

    trace = go.Scatter3d(
        x=x,
        y=y,
        z=z,
        mode=mode,
        line= line,
        marker=marker,
        visible=visible,
        showlegend=showlegend,
        legendgroup=legendgroup
    )
    trace.name = name
    return trace

def wireframe(surface, color = '#0066FF', width = 1, grid = True, name = None):
    cols = surface.keys()
    x = surface[cols[0]].reshape((surface.ni,surface.nj))
    y = surface[cols[1]].reshape((surface.ni,surface.nj))
    z = surface[cols[2]].reshape((surface.ni,surface.nj))
    
    lines = []
    line_marker = dict(color=color, width=width)
    for i, j, k in zip(x, y, z):
        lines.append(go.Scatter3d(x=i, y=j, z=k, mode='lines', line=line_marker,name = name))
    if grid:
        for i, j, k in zip(x.T, y.T, z.T):
            lines.append(go.Scatter3d(x=i, y=j, z=k, mode='lines', line=line_marker,name = name))
    return lines

def surface_mesh(surface, legendgroup = '', name = 'mesh', opacity = 1, showscale=False, cmin = None, cmax = None, hidesurface = False):
    cols = surface.keys()
    x = surface[cols[0]].reshape((surface.ni,surface.nj))
    y = surface[cols[1]].reshape((surface.ni,surface.nj))
    z = surface[cols[2]].reshape((surface.ni,surface.nj))
    surf = go.Surface(x=x, y=y, z=z, name = name,
                      showscale=showscale,
                      showlegend=True,
                      legendgroup = legendgroup,
                      # contours = {'x':{'show':True, 'color':"#444", 'usecolormap':False},
                      #             'y':{'show':True, 'color':"#444", 'usecolormap':False},
                      #             'z':{'show':True, 'color':"#444", 'usecolormap':False},
                      #            },
                      surfacecolor = x, 
                      colorscale= 'Viridis',
                      type = 'surface', 
                      cmin= cmin, cmax = cmax,
                      visible = not hidesurface,
                      hidesurface=hidesurface,
                      opacity=opacity)
    return surf


def analysis_traces(glocer_dir, input_file, args, visible = True):
    neg_null_file = glocer_dir+input_file+"_NegNulls.dat"
    pos_null_file = glocer_dir+input_file+"_PlusNulls.dat"
    surface1_file = glocer_dir+input_file+"_Surface1.dat"
    surface2_file = glocer_dir+input_file+"_Surface2.dat"
    separatrix_line_file = glocer_dir+input_file+"_Separator_Plane_Y.dat"

    neg_null_points = read_tecplot(neg_null_file)
    pos_null_points = read_tecplot(pos_null_file)

    if args.resolution:
        ni,nj = args.resolution
    else:
        ni, nj = 100,100

    # t = time.clock()
    surface1 = reduce_surface(read_tecplot(surface1_file), ni, nj)
    surface2 = reduce_surface(read_tecplot(surface2_file), ni, nj)
    # print ' surface reductions dt:', time.clock() - t

    # print 'generating interactive plot traces for', glocer_dir + input_file


    data = [plot_scatter(*list(neg_null_points[col] for col in neg_null_points.columns), color = 'blue', size = 6, legendgroup = 'positive nulls', name ='neg_nulls', visible=visible), 
            plot_scatter(*list(pos_null_points[col] for col in pos_null_points.columns),color = 'red', size = 6, legendgroup = 'negative nulls', name ='pos_nulls', visible=visible),
            ] 

    if os.path.isfile(separatrix_line_file):
        separatrix_line = read_tecplot(separatrix_line_file)
        data += [plot_scatter(*list(separatrix_line[col] for col in separatrix_line.columns),color='green',size = 2, legendgroup = 'separatrix line', name='separatrix',visible=visible, mode='marker')]


    data += [surface_mesh(surface1, legendgroup = 'separators', name='inner_separator', opacity =.9, showscale=False, hidesurface=not visible),
            surface_mesh(surface2, legendgroup = 'separators', name='outer_separator', opacity =.9, showscale=False, hidesurface=not visible)]


    # print get_ranges(data)

    return data

def get_ranges(data):
    x, y, z = [], [], []
    for d in data:
        x += [d.x.min(), d.x.max()]
        y += [d.y.min(), d.y.max()]
        z += [d.z.min(), d.z.max()]

    return (min(x),max(x)),(min(y),max(y)), (min(z),max(z))

"""Creates legend objects for group names mapped to colors"""
def get_legend_traces(colors_dict, size = 6):
    print colors_dict
    legend_traces = []
    for group_name in colors_dict:
        legend_traces.append(plot_scatter([None],[None],[None],
                                visible=True, showlegend=True, 
                                size = size, color=colors_dict[group_name],
                                name=group_name.upper(), legendgroup=group_name, 
                                line_color=None
                                )
        )
    return legend_traces

def get_layout(title, args):
    if args.figure_size:
        width, height = args.figure_size
    else:
        width, height = 600,600
    layout = go.Layout(
        title=title,
        autosize= False,
        
        margin=go.Margin( 
            l=0,
            r=0,
            b= 0,
            t=40,
            pad= 4
          ),
        width=width,
        height=height,
        showlegend=True,

        scene=dict(
            aspectmode="cube",
            xaxis=dict(
                autorange=False,
                range= args.xrange,
                gridcolor='rgb(255, 255, 255)',
                zerolinecolor='rgb(255, 255, 255)',
                showbackground=True,
                backgroundcolor='rgb(230, 230,230)'
            ),
            yaxis=dict(
                autorange='false',
                range=args.yrange,
                gridcolor='rgb(255, 255, 255)',
                zerolinecolor='rgb(255, 255, 255)',
                showbackground=True,
                backgroundcolor='rgb(230, 230,230)'
            ),
            zaxis=dict(
                autorange='false',
                range=args.zrange,
                gridcolor='rgb(255, 255, 255)',
                zerolinecolor='rgb(255, 255, 255)',
                showbackground=True,
                backgroundcolor='rgb(230, 230,230)'
            )
        ),
    )
    return layout

def multi_separator_html():
    html_src = """
       <!DOCTYPE html>
        <html>
        <head>
        <!--<meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        <script src="http://andreruffert.github.io/rangeslider.js/assets/rangeslider.js/dist/rangeslider.min.js"></script>

        <script src="analysis_timeseries.js"></script>
        <script src="analysis_layout.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
        <style type="text/css">
        /*  body {
              color: #404040;
              padding: 50px;
              max-width: 800px;
              margin: 0 auto;
            }*/

            h2 {
              font-weight: 300;
              text-align: center;
            }

        #slider{
            display: block;
            /*background:#7f7f7f;*/
        }
        .fancy {
            margin-top: 0;
            text-align: center;
            width: 50%;
            margin: 0 auto;
            /*background:#eee;*/
            display:block;
            /*margin: 10px auto;*/
        /*      -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;*/
        }
        </style>

        <script>

        function myclick() {
            var plotDiv = document.getElementById('myDiv');
            if (isNaN(document.getElementById("myslider").value)) {
                console.log('using previous');
            } else {
                window.slider_val = document.getElementById("myslider").value;
                window.t = window.slider_val;
            }
            // console.log(window.legend_visibility);

            // get current visibility
            for (var i in window.previous_trace_range) {
                window.legend_visibility[plotDiv.data[i]['legendgroup']] = plotDiv.data[i]['visible'];
            }

            var click_traces = analysis_timeseries[window.timesteps[Number(window.t)]];
            var trace_range = _.range(click_traces.length);

            // set visibility for new traces
            for (var i in trace_range) {
                click_traces[i]['visible'] = window.legend_visibility[click_traces[i]['legendgroup']];
            }
            
            // console.log('previous trace range:' + window.previous_trace_range);
            Plotly.deleteTraces(plotDiv,window.previous_trace_range); //remove [0,1,2,3,4... number of traces]
            Plotly.addTraces(plotDiv,click_traces,trace_range); // insert into [0,2,3..]
            Plotly.relayout(plotDiv, {title:window.timesteps[Number(window.t)]});
            window.previous_trace_range = trace_range;
            // console.log('new trace range: ' + window.previous_trace_range);
        };


        // Shorthand for $( document ).ready()
        $(function() {
            // var d3 = Plotly.d3;
            // var WIDTH_IN_PERCENT_OF_PARENT = 80,
            //     HEIGHT_IN_PERCENT_OF_PARENT = 50;
            // var gd3 = d3.select("div[id='myDiv']")
            //     .style({
            //         width: WIDTH_IN_PERCENT_OF_PARENT + '%',
            //         'margin-left': (100 - WIDTH_IN_PERCENT_OF_PARENT) / 2 + '%',
            //         height: HEIGHT_IN_PERCENT_OF_PARENT + 'vh',
            //         'margin-top': (100 - HEIGHT_IN_PERCENT_OF_PARENT) / 2 + 'vh'
            //     });
            // var myDivNode = gd3.node();
            // window.onresize = function() { Plotly.Plots.resize( myDivNode ); };

            window.hidesurface = {
            hidesurface: [true]
            };
            window.hidescatter = {
                visible: [false]
            };
            window.showsurface = {
                hidesurface: [false]
            };
            window.showscatter = {
                visible:[true]
            };

            console.log("analysis_timeseries:" + Object.keys(analysis_timeseries).length);
            document.getElementById("myslider").max = Object.keys(analysis_timeseries).length -1;

            window.timesteps = Object.keys(analysis_timeseries);
            window.timesteps.sort();
            
            window.t = 0;

            window.default_slider_val = "0";
            window.slider_val = 0;

            var $plotDiv = $("#myDiv")[0];

            window.traces_per_frame = analysis_timeseries[window.timesteps[0]].length; //not a constant

            window.previous_trace_range = _.range(window.traces_per_frame);
            console.log('initial trace range:' + window.previous_trace_range);

            window.data = [];

            window.data.push.apply(window.data,analysis_timeseries[window.timesteps[0]]);

            if ('undefined' !== typeof earth) {
                window.data.push.apply(window.data,earth);
            }

            if ('undefined' !== typeof legend) {
                window.data.push.apply(window.data,legend);
            }

            window.layout = layout;
            Plotly.newPlot('myDiv', window.data, window.layout);

            window.legend_visibility = {};

            for (var i in window.previous_trace_range) {
                window.legend_visibility[$plotDiv.data[i]['legendgroup']] = $plotDiv.data[i]['visible'];
            }


        });

        </script>

        </head>
        <body>

        <div id="parent" style="height:600px" class="fancy">
            <div id="myDiv" style="height: 600px; margin: 0 auto;"><!-- Plotly chart will be drawn inside this DIV --></div>    
        </div>
        <div id="parent2" class="fancy">
            <input type="range" name="slider" id="myslider" value="0" min="0" max="1" animate="True" onchange="myclick()">
        </div>


        </body>
        </html>


     """

    return html_src

def main(argv):
    parser = argparse.ArgumentParser(description="Visualizes results from Glocer Separatrix Finder.")
    parser.add_argument("-input_file", metavar = 'input_data_file', type=str, help="name of data file used to generate results")
    parser.add_argument("-glocer_dir", metavar = 'full/path/to/glocer_dir', type=str, help="path to output directory of glocer analysis")
    parser.add_argument("-res", "--resolution", type = int, nargs = '+', metavar=('ni','nj',), help = "resolution of the vis meshes")
    parser.add_argument("-fig_size", "--figure_size", type = int, nargs = '+', metavar=('width','height'), help = "shape of the figure in pixels")
    parser.add_argument("-auto_open", "--auto_open_in_browser", action="store_true", help = 'opens the visualization in the browser') 
    parser.add_argument("-out_dir", "--output_directory", type = str, help = "directory in which to place visualizer. If not specified, visualizer will be placed in glocer_dir")
    parser.add_argument("-show_earth", action="store_true", help = 'include a rendering of earth in ouput')
    parser.add_argument("-xrange", type = float, nargs = 2, metavar = ('xmin','xmax'), default = [-20, 20], help = "range of x for 3D plot")
    parser.add_argument("-yrange", type = float, nargs = 2, metavar = ('xmin','xmax'), default = [-20, 20], help = "range of x for 3D plot")
    parser.add_argument("-zrange", type = float, nargs = 2, metavar = ('xmin','xmax'), default = [-20, 20], help = "range of x for 3D plot")
    parser.add_argument("-spec_const", "--special_constellations", type = str, nargs = '+', default = ['artemis', 'de',' demeter', 'dmsp', 'arie'], help = "special constellations names that may not be detected")
    args = parser.parse_args()

    # set up satellite colors
    sat_colors = OrderedDict()
    color_scale = cl.scales['12']['qual']
    color_list = color_scale['Paired'] + color_scale['Set3'] #two sets of 12 unique colors

    if args.input_file:
        if args.glocer_dir:
            glocer_dir = args.glocer_dir
        else:
            input_file = os.path.abspath(args.input_file) # full/path/to/file.out
            glocer_dir = os.path.dirname(input_file) + '/' # full/path/to/
            input_file = input_file.split('/')[-1] # file.out
        data = analysis_traces(glocer_dir, input_file, args)
        if args.show_earth:
            data += earth_plot()
        satellite_file = glocer_dir+input_file+"_Satellites.dat"
        if os.path.isfile(satellite_file):
            sat_traces = Sat_vis.load_satellite_traces(satellite_file, sat_colors, color_list, 
                                                    special_constellations = args.special_constellations)
            data += sat_traces + get_legend_traces(sat_colors)

        fig = go.Figure(data=data, layout=get_layout(input_file, args))

        if args.output_directory:
            output_directory = args.output_directory
        else:
            output_directory = glocer_dir

        plot(fig, 
            filename=output_directory+input_file+'_Visualizer.html', 
            auto_open=args.auto_open_in_browser, 
            show_link = False)

    elif args.glocer_dir:
        glocer_dir = args.glocer_dir
        if args.output_directory:
            output_directory = args.output_directory
        else:
            output_directory = glocer_dir

        analysis_dict = OrderedDict()

        print 'generating time series..'

        for i in sorted(os.listdir(args.glocer_dir)):
            if i.endswith("_Surface1.dat"): 
                input_file = i.split("_Surface1.dat")[0]
                # print input_file
                analysis_dict[input_file] = analysis_traces(glocer_dir, input_file, args, visible=True)
                satellite_file = glocer_dir+input_file+"_Satellites.dat"
                if os.path.isfile(satellite_file):
                    sat_traces = Sat_vis.load_satellite_traces(satellite_file, sat_colors, color_list, 
                                                        special_constellations = args.special_constellations)
                    print sat_traces
                    analysis_dict[input_file] += sat_traces

        # print 'total timesteps:', len(analysis_dict)

        with open(output_directory+'analysis_layout.js', 'w') as f:
            f.write('var layout = ' + json.dumps(get_layout(analysis_dict.keys()[0],args), cls=PlotlyJSONEncoder))
            if args.show_earth:
                f.write(';\n var earth = ' + json.dumps(earth_plot(), cls=PlotlyJSONEncoder))
            if len(sat_colors.keys()) > 0:
                legend_traces = get_legend_traces(sat_colors)
                f.write(';\n var legend =' + json.dumps(legend_traces))
        with open(output_directory+'analysis_timeseries.js', 'w') as g:
            g.write('var analysis_timeseries = ' + json.dumps(analysis_dict, cls=PlotlyJSONEncoder))

        with open(output_directory+'multi_separators.html', 'w') as h:
            h.write(multi_separator_html())

        if args.auto_open_in_browser:
            import webbrowser
            webbrowser.open_new_tab('file://' + os.path.realpath(output_directory + 'multi_separators.html'))

    else: raise IOError('no glocer_dir or input_file specified')


if __name__ == '__main__':
	main(sys.argv[1:])