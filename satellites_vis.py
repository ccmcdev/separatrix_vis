import Glocer_visualizer
from collections import OrderedDict, defaultdict
import colorlover as cl
import plotly.graph_objs as go
from plotly.offline import plot
import sys
import os

def get_prefix_names(names, special_constellations = ['artemis']):
    '''This Will still fail for cases where the prefix is the satellite name! (z1artemis and z2artemis)'''
    sat_names = defaultdict(list)
    found_special = []
    names.sort()
    
    # check special names
    special_constellations.sort(key=len, reverse=True) # sorted by longest first
    for i,name in enumerate(names):
        for special in special_constellations: 
            if name.find(special) == 0:
                sat_names[special].append(name)
                found_special.append(i)
                break

        
    names = [names[i] for i in range(len(names)) if i not in found_special]
    prefix_matches = []
    min_prefix_length = 3
    start = 0

	# look for [name, newname..] in [name1, name2, newname21, newname22..]
    for i in range(len(names)+1):
        for end in range(start+1,len(names)+1): 
            result = os.path.commonprefix(names[start:end])
            if len(result) < min_prefix_length: #no match for [name1, name2, newname]
                while last_result[-1].isdigit(): #remove common end digits
                	last_result = last_result[:-1]
                sat_names[last_result] = names[start:end-1]
                start = end-1
                prefix_matches.append(last_result)
                break
            last_result = result
        if end >= len(names): 
            sat_names[last_result] = names[start:end]
            break
    sat_names = OrderedDict((key, sat_names[key]) for key in sorted(sat_names.keys()))
    return sat_names

def update_color_dict(names, color_list):
	pass

def load_satellite_traces(filename, sat_colors, color_list, size = 4, special_constellations = ['artemis']):
	print 'satellite filename:', filename
	satellites = Glocer_visualizer.read_tecplot(filename)

	sat_groups = get_prefix_names(satellites.NAME.tolist(), special_constellations)

	for i, sat in enumerate(sat_groups):
		if sat_colors.has_key(sat):
			pass
		else: # assign colors, reuse if over length of color list
			sat_colors[sat] = color_list[len(sat_colors)%len(color_list)] 

	# sat_color keys may be larger than sat_groups

	satellite_traces = []
	for group_name, names in sat_groups.items(): 
	    group = satellites[satellites.NAME.isin(names)]
	    for i in group.index:
	        satellite_traces.append(
	            Glocer_visualizer.plot_scatter([group.X[i]], [group.Y[i]],[group.Z[i]],
	                         visible=True, size = size, color=sat_colors[group_name], 
	                         name =group.NAME[i], legendgroup=group_name, 
	                         showlegend=False, line_color = 'black'
	                         )
	            )

	return satellite_traces

def plot_satellites(traces):
	layout = go.Layout(
	    title='Satellites',
	    width=800,
	    height=800,
	    scene=dict(
	        aspectmode='cube',
	        xaxis=dict(
	            range=[-50, 50],
	            gridcolor='rgb(255, 255, 255)',
	            zerolinecolor='rgb(255, 255, 255)',
	            showbackground=True,
	            backgroundcolor='rgb(230, 230,230)'
	        ),
	        yaxis=dict(
	            range=[-50, 50],
	            gridcolor='rgb(255, 255, 255)',
	            zerolinecolor='rgb(255, 255, 255)',
	            showbackground=True,
	            backgroundcolor='rgb(230, 230,230)'
	        ),
	        zaxis=dict(
	            range=[-50, 50],
	            gridcolor='rgb(255, 255, 255)',
	            zerolinecolor='rgb(255, 255, 255)',
	            showbackground=True,
	            backgroundcolor='rgb(230, 230,230)'
	        )
	    )
	)
	fig = go.Figure(data=traces, layout=layout)
	plot(fig,filename='satellites.html')


def main(argv):
	color_scale = cl.scales['12']['qual']
	color_list = color_scale['Paired'] + color_scale['Set3'] #two sets of 12 unique colors
	sat_colors = OrderedDict()

	sat_traces = load_satellite_traces('sample_data/3d__var_1_e20030923-105600-000.out_Satellites.dat',
										sat_colors, color_list)
	
	legend_traces = []
	for group_name in sat_colors:
		legend_traces.append(Glocer_visualizer.plot_scatter([None],[None],[None],
								visible=True, showlegend=True, size = 6, 
								color=sat_colors[group_name],
								name=group_name.upper(), legendgroup=group_name, line_color=None)
							)
	plot_satellites(sat_traces + legend_traces)


if __name__ == '__main__':
	main(sys.argv[1:])