
import sys
import pandas as pd
import numpy as np
import plotly.graph_objs as go
from plotly.offline import plot


def get_segments(trace_x,trace_y,trace_z,every=10):
    line, lines = [], []
    count = 0
    for x,y,z in zip(trace_x, trace_y, trace_z):
        vec = np.array([x,y,z])
        x,y,z = vec/np.linalg.norm(vec)
        if np.isnan(x):
            if len(line[0::every]) > 0:
                lines.append(line[0::every])
                count+=len(lines[-1])
                line = []
            else: pass
        else:
            line.append(np.array([x,y,z]))
    if len(line)>0:
        lines.append(line)
        count+=len(lines[-1])
    return lines

def downsample(lines, resolution=.02, closed = False):
    new_lines = []
    count = 0
    for line in lines:
        s = 0
        p0 = line[0]
        new_line = [p0]
        for p1 in line[1:]:
            s += np.linalg.norm(p1-p0)
            if s > resolution:
                new_line.append(p1)
                s=0
            p0=p1
        if closed:
            new_line.append(line[0])
        else:
            new_line.append(line[-1])
        if len(new_line) > 2:
            count += len(new_line)
            new_lines.append(new_line)
    return new_lines

def plot_segments(x,y,z,color,width,every=10,resolution=.02, closed = False,name = None):
    segments = get_segments(x,y,z,every)
    segments = downsample(segments,resolution, closed)
    lines = []
    line_marker = dict(color=color, width=width)
    for segment in segments:
        x,y,z = zip(*segment)
        lines.append(go.Scatter3d(x=x, y=y, z=z, mode='lines', line=line_marker, legendgroup = 'Earth', showlegend=False,name = name))
    return lines


def earth_plot(earth_file='earth.csv.gz',width=3,res_continents = .1, res_rivers = .05):
    earth = pd.read_csv(earth_file,sep=',')
    cols = earth.keys()
    for col in cols:
        earth[col] =pd.to_numeric(earth[col],'coerce')
    continents = plot_segments(earth.continents_x,earth.continents_y,earth.continents_z, 
        'green', width,1,res_continents, True,'Earth')
    rivers = plot_segments(earth.rivers_x,earth.rivers_y,earth.rivers_z, 'blue', 1,1,res_rivers, False, 'Earth')
    earth_legend = go.Scatter3d(x=[None],y=[None],z=[None], mode = 'markers',
                                visible=True, showlegend=True, 
                                name='Earth', legendgroup='Earth',
                                marker = dict(
                                    size=6,
                                    color = 'blue',
                                    colorscale='Viridis',
                                    line = {'width':2}
                                    ),
                                line = {'width':2}
                            )
                
    return continents + rivers + [earth_legend]


def main(argv):
    plot(earth_plot(res_continents=.01, res_rivers = .01), show_link=False, filename = 'earth_plot.html')

if __name__ == "__main__":
    main(sys.argv[1:])
